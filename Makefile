platform = unix
ifeq ($(OS),Windows_NT)
	platform = win
	EXE = .exe
endif

CC = gcc
TOOLS := bin/a09$(EXE) bin/bmpconv$(EXE) bin/d09$(EXE)
PROJECTS := retromachine
EXAMPLES := bin/asm_rom.bin bin/c_rom.bin bin/tetris.bin bin/z80_rom.bin

CPU = 6809
#CPU = Z80
.PHONY: all demos clean help $(PROJECTS)

all: $(TOOLS) $(PROJECTS) 
demos: $(all) $(EXAMPLES)

bin/a09$(EXE): source/tools/a09.c 
	echo $(platform)
	$(CC) source/tools/a09.c -Os -s -obin/a09$(EXE)

bin/bmpconv$(EXE): source/tools/bmpconv.c
	$(CC) source/tools/bmpconv.c -Os -s -obin/bmpconv$(EXE)

bin/d09$(EXE): 
	$(CC) source/tools/f9dasm.c -Os -s -obin/d09$(EXE)

demos/assets/blacktiger_map.bin: images/BlackTiger256.bmp
	bin/bmpconv -odemos/assets/blacktiger -m -c -p4 -r images/BlackTiger256.bmp

demos/assets/font1bit_tiles.bin: images/5x6font.bmp
	bin/bmpconv -odemos/assets/font1bit -p1 images/5x6font.bmp

bin/asm_rom.bin: demos/assets/blacktiger_map.bin demos/assets/font1bit_tiles.bin demos/asm_demo/bt.asm
	bin/a09 demos/asm_demo/bt.asm -mdemos/asm_demo/bt.map -Ldemos/asm_demo/bt.lst -Bbin/asm_rom.bin

bin/c_rom.bin: demos/c_demo/bootrom.c
	@${MAKE} -C demos/c_demo -f Makefile

bin/tetris.bin: demos/tetris/bootrom.c
	@$(MAKE) -C demos/tetris -f Makefile

bin/z80_rom.bin: demos/z80/bootrom.asm 
	@$(MAKE) -C demos/z80 -f Makefile


#ROM: bootrom/basic.asm
	#bin/a09 bootrom/basic.asm -bootrom/rom.lst -Bbootrom/rom.rom

retromachine: 
	@${MAKE} --no-print-directory -C . -f Makefile.retro CPU=$(CPU)

clean:
	@${MAKE} --no-print-directory -C . -f Makefile.retro clean
	rm -f example/blacktiger*
	rm -f bin/bmpconv
	rm -f bin/a09
	rm -f bin/*.bin
	
	

