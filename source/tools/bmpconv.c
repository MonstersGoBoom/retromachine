#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "stretchy_buffer.h"

#define SWAP_UINT16(x) (((x) >> 8) | ((x) << 8))

typedef struct
{
	uint16_t tile:10;
	uint16_t hflip:1;
	uint16_t vflip:1;
	uint16_t clut:4;
} MY_TILE;

typedef struct
{
	uint16_t tile:11;
	uint16_t hflip:1;
	uint16_t vflip:1;
	uint16_t clut:2;
	uint16_t priority:1;
} SEGA_TILE;


//	output base name
char *base_name;
bool checkRepeats,outMap,outClut,outSega,outMapAsCoords;

//
//	BMP handling 
//

uint16_t mw,mh;
uint16_t *map;
uint8_t planes;

#pragma pack(1)
typedef struct
{
	uint16_t magic;
	uint32_t fileSize;
	uint32_t reserved0;
	uint32_t bitmapDataOffset;

	uint32_t bitmapHeaderSize;
	uint32_t width;
	uint32_t height;
	uint16_t planes;
	uint16_t bitsPerPixel;
	uint32_t compression;
	uint32_t bitmapDataSize;
	uint32_t hRes;
	uint32_t vRes;
	uint32_t colors;
	uint32_t importantColors;
} BMPHeader_t;

void savefile(const char *fname,uint8_t *ptr,uint32_t size)
{
	FILE *fp=fopen(fname,"wb");
	fwrite(ptr,size,1,fp);
	fclose(fp);
}

uint8_t * loadfile(const char *fname,uint32_t *size)
{
FILE *fp=fopen(fname,"rb");
uint32_t len;
uint8_t *buffer;

	if (fp==NULL)
	{
		printf("%s failed to load\n",fname);
		return NULL;
	}
	fseek(fp,0,SEEK_END);
	len = ftell(fp);
	fseek(fp,0,SEEK_SET);

	if (size!=NULL) *size=len;

	buffer=malloc(len);
	if (buffer==NULL)
	{
		printf("alloc failed\n");
		return NULL;
	}
	fread(buffer,len,1,fp);
	fclose(fp);
	printf("load %s\n",fname);
	return buffer;	
}

FILE *tiles_fp;
BMPHeader_t		*bmp_header;
uint8_t				*bmp_data;
typedef struct {
	uint16_t b:4;
	uint16_t a:4;
	uint16_t r:4;
	uint16_t g:4;
} RETRO_COLOR;
// 


FILE *base_open(const char *fname,const char *mode)
{
FILE *fp;
char temp_name[256];

	sprintf(temp_name,"%s_%s.bin\0",base_name,fname);

	fp = fopen(temp_name,mode);

	if (fp==NULL)
	{
		printf("%s failed to open\n",temp_name);
		return NULL;
	}

	return fp;
}


void loadbmp(const char *fname)
{
	uint8_t *buffer = loadfile(fname,NULL);

	bmp_header = NULL;
	if (buffer!=NULL)
	{
		bmp_header = (BMPHeader_t*)&buffer[0];
		unsigned char *colors = &buffer[0]+sizeof(BMPHeader_t);

		if (bmp_header->bitsPerPixel>8)
		{
			printf("required 8bpp bmp\n");
			bmp_header = NULL;
			return;
		}

		if (bmp_header->bitsPerPixel==4)
		{
			uint8_t *src = &buffer[bmp_header->bitmapDataOffset];
			printf("4bpp bmp expand %x %x\n",src-buffer,buffer);

			bmp_data = (uint8_t*)malloc(bmp_header->width*bmp_header->height);

			for (int y=0;y<bmp_header->height;y++)
			{
				for (int x=0;x<bmp_header->width;x++)
				{
					uint8_t byte=src[(x>>1)+(y*(bmp_header->width>>1))];
					if ((x&1)==0) byte>>=4;					
					bmp_data[x+(y*bmp_header->width)]=byte&0xf;
				}
			}
		}
		else 
		{
			bmp_data = &buffer[bmp_header->bitmapDataOffset];
		}

		if (outClut==true)
		{
			FILE *fp=base_open("clut","wb");
			for (int q=0;q<bmp_header->colors;q++)
			{
				RETRO_COLOR rc;
				if (outSega==true)
				{
					rc.r=colors[(q*4)+2]>>4;
					rc.g=colors[(q*4)+1]>>4;
					rc.b=colors[(q*4)+0]>>4;
					fwrite(&rc,sizeof(uint16_t),1,fp);
				}
				else 
				{
					rc.r=colors[(q*4)+0]>>4;
					rc.g=colors[(q*4)+1]>>4;
					rc.b=colors[(q*4)+2]>>4;
					fwrite(&rc,sizeof(uint16_t),1,fp);
				}
			}
			fclose(fp);
		}
	}
}


typedef struct 
{
	uint32_t 	crc;
	uint8_t 	clut;
	uint8_t bytes[8*8];
} TILE;

TILE **tiles;

void tiles_init()
{
//	TILE *block=malloc(sizeof(TILE));
//	sb_push(tiles, block);
}
//	grab an 8x8 tile and check against our current list of tiles
//	if we're a repeat just return that index
//	add the new ones to our list 
//	
uint16_t tile_grab(int x,int y)
{
TILE b;

	b.crc = 0;
	b.clut = 0;
	for (int _y=0;_y<8;_y++)
	{
		for (int _x=0;_x<8;_x++)
		{
			uint8_t byte=bmp_data[(x+_x) + (((bmp_header->height-1)-(y+_y))*bmp_header->width)];	//	only bottom 4 bits 
			uint8_t clut= (byte>>4)&0xf;
			if (((byte&0xf)!=0) && (b.clut==0))
				b.clut=clut;
			b.crc^=byte&0xf;
			b.bytes[_x+(_y*8)]=byte&0xf;
		}
	}

	uint16_t ret=sb_count(tiles);
	
	if (checkRepeats==true)
	{
		for (uint16_t q=0;q<sb_count(tiles);q++)
		{
			//	good chance of match
			if (tiles[q]->crc==b.crc)
			{
				int mdiffer = 0;

				//	check fully to be sure 
				for (int _x=0;_x<8*8;_x++)
					if (tiles[q]->bytes[_x]!=b.bytes[_x])
						mdiffer++;

				//	nope it's exactly the same , but the clut could be different 
				if (mdiffer==0)			
				{
					uint16_t ret=q&0x7ff;
					ret|=b.clut<<12;
					return ret;
				}	
				else 
				{
					//	check xflip
					mdiffer = 0;
					for (int _y=0;_y<8;_y++)
						for (int _x=0;_x<8;_x++)
							if (tiles[q]->bytes[_x+(_y*8)]!=b.bytes[(7-_x)+(_y*8)])
								mdiffer++;

					if (mdiffer==0)			
					{
						uint16_t ret=q&0x7ff;
						ret|=b.clut<<12;
						ret|=1<<10;
						return ret;
					}

					//	check yflip
					mdiffer = 0;
					for (int _y=0;_y<8;_y++)
						for (int _x=0;_x<8;_x++)
							if (tiles[q]->bytes[_x+(_y*8)]!=b.bytes[_x+((7-_y)*8)])
								mdiffer++;

					if (mdiffer==0)			
					{
						uint16_t ret=q&0x7ff;
						ret|=b.clut<<12;
						ret|=1<<11;
						return ret;
					}

					//	check yflip & xflip
					mdiffer = 0;
					for (int _y=0;_y<8;_y++)
						for (int _x=0;_x<8;_x++)
							if (tiles[q]->bytes[_x+(_y*8)]!=b.bytes[(7-_x)+((7-_y)*8)])
								mdiffer++;

					if (mdiffer==0)			
					{
						uint16_t ret=q&0x7ff;
						ret|=b.clut<<12;
						ret|=1<<10;
						ret|=1<<11;
						return ret;
					}
				}
			}
		}
	}

	if (outSega==true)
	{
		for (int _y=0;_y<8;_y++)
		{
			uint8_t *ptr=&b.bytes[(_y*8)];
			uint8_t byte=0;
			for (int x=0;x<4;x++)
			{
				byte=(*ptr++&0xf)<<4;
				byte|=(*ptr++&0xf);
				fwrite(&byte,1,1,tiles_fp);
			}
		}
	}
	else 
	{
		for (int plane=0;plane<planes;plane++)
		{
			uint8_t bits[5]={1,2,4,8};
	//		printf("; tile: $%x plane: %x %d\n",	ret,plane,bits[plane]);
	//		fprintf(tiles_fp," fcb ");
			for (int y=0;y<8;y++)
			{
				uint8_t byte=0;
				for (int x=0;x<8;x++)
				{
					if ((b.bytes[x+(y*8)]>>plane)&1!=0)
					{
						byte|=1<<(7-x);
					}	
				}
	//			if (y==7) 
	//				fprintf(tiles_fp,"$%02x\n",	byte);
	//			else 
	//				fprintf(tiles_fp,"$%02x,",	byte);
				fwrite(&byte,1,1,tiles_fp);
			}
		}
	}

#if 0
	//	write it to our file

	if (asC==false)
	{
		fprintf(tiles_fp,"; TILE %d\n",ret);
		for (int l=0;l<8;l++)
		{
			int line = l*8;
			fprintf(tiles_fp," fcb $%x%x,$%x%x,$%x%x,$%x%x\n",	b.bytes[(line)+0],b.bytes[(line)+1],
																													  b.bytes[(line)+2],b.bytes[(line)+3],
																													  b.bytes[(line)+4],b.bytes[(line)+5],
																													  b.bytes[(line)+6],b.bytes[(line)+7]);
		}
	}
	else
	{
		if (ret<256)
		{
			fprintf(tiles_fp,"// TILE %d\n",ret);
			for (int l=0;l<8;l++)
			{
				int line = l*8;

				fprintf(tiles_fp,"	0x%x%x,0x%x%x,0x%x%x,0x%x%x,\n",	b.bytes[(line)+0],b.bytes[(line)+1],
																													b.bytes[(line)+2],b.bytes[(line)+3],
																													b.bytes[(line)+4],b.bytes[(line)+5],
																													b.bytes[(line)+6],b.bytes[(line)+7]);
			}																												
		}
	}
#endif
	//	add to list 
	//fwrite(&b.bytes[0],32,1,tiles_fp);

	TILE *copy = malloc(sizeof(TILE));
	memcpy(copy,&b,sizeof(TILE));
	sb_push(tiles,copy);
	ret&=0x7ff;
	ret|=b.clut<<12;
	return ret;
}

const char *get_argument(const char *input)
{
	if (input==NULL) return NULL;
	const char *dot = strrchr(input, '-');
	if(!dot) return NULL;
	return dot;
}

void process_file(const char *name)
{
	printf("try process %s\n",name);
	loadbmp(name);
	if (bmp_header!=NULL)
	{
		printf("width %x\n",bmp_header->width);
		printf("height %x\n",bmp_header->height);
		mw = bmp_header->width>>3;
		mh = bmp_header->height>>3;
		map = (uint16_t*)malloc((mw*sizeof(uint16_t))*mh);

		tiles_init();

		tiles_fp=base_open("tiles","wb");

		for (int y=0;y<mh;y++)
		{
			for (int x=0;x<mw;x++)
			{
//				map[x+(y*mw)]=SWAP_UINT16(tile_grab(x*8,y*8));
				map[x+(y*mw)]=(tile_grab(x*8,y*8));
			}
		}
		fclose(tiles_fp);

		printf("%d tiles\n",sb_count(tiles));
		printf("map %d * %d\n",mw,mh);

		for (int q=0;q<sb_count(tiles);q++)
			free(tiles[q]);
		sb_free(tiles);

		if ((outMapAsCoords==true) && (outMap==false))
		{
			FILE *fp=base_open("mapc","wt");
			for (uint16_t y=0;y<mh;y++)
			{
				for (uint16_t x=0;x<mw;x++)
				{
					uint16_t out=map[	(y * mw) + x]&0x3ff;
					if (out!=0)
					{
						uint16_t blank=0;
						fwrite(&out,2,1,fp);
						fwrite(&x,2,1,fp);
						fwrite(&y,2,1,fp);
						fwrite(&blank,2,1,fp);
//						fprintf(fp,"id:%d x:%x y:%x\n",out&0x3ff,x,y);
					}
				}
			}
			fclose(fp);
		}
		if ((outMap==true) && (outMapAsCoords==false))
		{
			FILE *fp=base_open("map","wb");

			for (int y=0;y<mw*mh;y++)
			{
				uint16_t out=map[y];
				MY_TILE *m=(MY_TILE*)&map[y];
//				if (out!=0)
//					printf("%04X ID %X X %X Y %x CLUT %x\n",out,m->tile,m->hflip,m->vflip,m->clut);
//				if ((y&0x1f)==0)
//					fprintf(fp," fcw ");
				if (outSega==true)
				{
					SEGA_TILE st;
					uint16_t p;
					st.tile=m->tile;
					st.clut=m->clut&3;
					st.hflip=m->hflip;
					st.vflip=m->vflip;
					memcpy(&p,&st,sizeof(uint16_t));
					p=SWAP_UINT16(p);
					fwrite(&p,2,1,fp);					
				}
				else 
				{
					out=SWAP_UINT16(map[y]);
					fwrite(&out,2,1,fp);
				}
			}

			fclose(fp);
		}			
		free(map);
		free(bmp_header);
	}		
}

int main(int argc,char *argv[])
{
	base_name = "";
	outClut=false;
	outMap=false;
	outMapAsCoords=false;
	outSega=false;
	checkRepeats=false;
	planes = 4;
	for (int c=1;c<argc;c++)
	{
		const char *arg = get_argument(argv[c]);
		if (arg!=NULL)
		{
			if (strncmp(arg,"-o",2)==0)
			{
				base_name = (char*)arg+2;
				printf("%s\n",base_name);
			}
			if (strncmp(arg,"-p",2)==0)
			{
				uint8_t p = atoi((arg+2));
				if (p<1) p=1;
				if (p>4) p=4;
				planes=p;
				printf("planes %d\n",planes);
			}
			if (strncmp(arg,"-r",2)==0)
			{
				checkRepeats=true;
			}

			if (strncmp(arg,"-m",2)==0)
			{
				outMap=true;
			}
			if (strncmp(arg,"-t",2)==0)
			{
				outMapAsCoords=true;
			}

			if (strncmp(arg,"-c",2)==0)
			{
				outClut=true;
			}
			if (strncmp(arg,"-s",2)==0)
			{
				outSega=true;
			}
		}
		else
		{
			printf("%s\n",argv[c]);
			process_file(argv[c]);
		}
	}
}



/*			
*/		
