
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define CHIPS_IMPL
#include "Z80.h"
z80_t mZ80;

uint8_t read8(uint16_t addr);
void write8 (uint16_t address, uint8_t data);

int nmi_state;
int irq_state;

uint64_t cpu_tick(int num_ticks,uint64_t pins,void *user)
{
static uint16_t lread=0xffff,lwrite=0xffff;
	if (pins & Z80_MREQ)
	{
    const uint16_t addr = Z80_GET_ADDR(pins);

            // a regular memory access
		if (pins & Z80_RD) {
				Z80_SET_DATA(pins, read8(addr));
		}
		else {
				write8(addr,Z80_GET_DATA(pins));
		}
	}
	else if (pins & Z80_IORQ)
	{
/*		
		const uint16_t addr = Z80_GET_ADDR(pins)&0xff;
		const uint8_t data= Z80_GET_DATA(pins);
		if (pins & Z80_RD)
		{
			printf("Z80_SET_DATA %x %x\n",addr,data);
			Z80_SET_DATA(pins, read8(addr+0xff00));
		}
		else if (pins & Z80_WR)
		{
			pins |= Z80_INT;
			write8(addr+0xff00,data);				
			printf("IO write8 %x %x\n",addr,data);
		}
		else if (pins & Z80_M1)
		{
			Z80_SET_DATA(pins, 0xE0);
		}
*/		
	}
	if (nmi_state==1)
		pins|=Z80_NMI;
	else 
		pins &= ~Z80_NMI;

	if (irq_state==1)
		pins|=Z80_INT;
	else		
		pins &= ~Z80_INT;

	return pins;
}

void cpu_reset (void)
{
	z80_init(&mZ80,
						&(z80_desc_t){
        			.tick_cb = cpu_tick
    				});	
	nmi_state = 0;
	irq_state = 0;
	z80_reset(&mZ80);

}

extern uint32_t cpu_exec (void)
{
	z80_exec(&mZ80,1);
	nmi_state=0;
	irq_state=0;
}
extern void cpu_reset (void);

void cpu_nmi()
{
	nmi_state = 1;
}

void cpu_irq()
{
	irq_state = 1;
}

unsigned emu_store_state(void *data)
{
	return 0x28;
}

void emu_load_state(void *data)
{
}

void cpu_printf()
{
}

