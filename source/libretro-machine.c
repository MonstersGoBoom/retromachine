#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>

#include "libretro.h"

#include "machine.h"


void machine_init();
void machine_deinit();
void machine_ctrl(uint32_t pad);
void machine_update(uint32_t *buffer,uint32_t stride);

#include "cpu.h"

extern uint8_t ram[65535*4];	//	max size 

static uint32_t *frame_buf;
static struct retro_log_callback logging;
static retro_log_printf_t log_cb;

static void fallback_log(enum retro_log_level level, const char *fmt, ...)
{
   (void)level;
   va_list va;
   va_start(va, fmt);
   vfprintf(stderr, fmt, va);
   va_end(va);
}

void retro_init(void)
{
	log_cb(RETRO_LOG_INFO, "Retro Init %d by %d\n",SCREEN_W,SCREEN_H);
	frame_buf = calloc(SCREEN_W * SCREEN_H, sizeof(uint32_t));
	machine_init();
}	

void retro_deinit(void)
{
	log_cb(RETRO_LOG_INFO, "Retro DeInit\n");
   free(frame_buf);
	machine_deinit();
   frame_buf = NULL;
}

unsigned retro_api_version(void)
{
   return RETRO_API_VERSION;
}

static void keyboard_cb(bool down, unsigned keycode,
      uint32_t character, uint16_t mod)
{
//   log_cb(RETRO_LOG_INFO, "Down: %s, Code: %d, Char: %u, Mod: %u.\n", down ? "yes" : "no", keycode, character, mod);
}

void retro_set_controller_port_device(unsigned port, unsigned device)
{
   log_cb(RETRO_LOG_INFO, "Plugging device %u into port %u.\n", device, port);
}

void retro_get_system_info(struct retro_system_info *info)
{
   memset(info, 0, sizeof(*info));
   info->library_name     = "Machine";
   info->library_version  = "v1";
   info->need_fullpath    = false;
   info->valid_extensions = NULL; // Anything is fine, we don't care.
}

static retro_video_refresh_t video_cb;
static retro_audio_sample_t audio_cb;
static retro_audio_sample_batch_t audio_batch_cb;
static retro_environment_t environ_cb;
static retro_input_poll_t input_poll_cb;
static retro_input_state_t input_state_cb;

void retro_get_system_av_info(struct retro_system_av_info *info)
{
   float aspect = 4.0f / 3.0f;
   float sampling_rate = 30000.0f;

   info->timing = (struct retro_system_timing) {
      .fps = 60.0,
      .sample_rate = sampling_rate,
   };

   info->geometry = (struct retro_game_geometry) {
      .base_width   = SCREEN_W,
      .base_height  = SCREEN_H,
      .max_width    = SCREEN_W,
      .max_height   = SCREEN_H,
      .aspect_ratio = aspect,
   };

}

void retro_set_environment(retro_environment_t cb)
{
   environ_cb = cb;

   bool no_content = true;

   cb(RETRO_ENVIRONMENT_SET_SUPPORT_NO_GAME, &no_content);

   if (cb(RETRO_ENVIRONMENT_GET_LOG_INTERFACE, &logging))
      log_cb = logging.log;
   else
      log_cb = fallback_log;
}

void retro_set_audio_sample(retro_audio_sample_t cb)
{
   audio_cb = cb;
}

void retro_set_audio_sample_batch(retro_audio_sample_batch_t cb)
{
   audio_batch_cb = cb;
}

void retro_set_input_poll(retro_input_poll_t cb)
{
   input_poll_cb = cb;
}

void retro_set_input_state(retro_input_state_t cb)
{
   input_state_cb = cb;
}

void retro_set_video_refresh(retro_video_refresh_t cb)
{
   video_cb = cb;
}

void retro_reset(void)
{
	log_cb(RETRO_LOG_INFO, "Reset\n");
}


uint32_t buttons[]={
	RETRO_DEVICE_ID_JOYPAD_SELECT ,
	RETRO_DEVICE_ID_JOYPAD_START , 
	RETRO_DEVICE_ID_JOYPAD_UP    ,
	RETRO_DEVICE_ID_JOYPAD_DOWN  ,
	RETRO_DEVICE_ID_JOYPAD_LEFT  ,
	RETRO_DEVICE_ID_JOYPAD_RIGHT ,
	RETRO_DEVICE_ID_JOYPAD_A,
	RETRO_DEVICE_ID_JOYPAD_B,
};

static void update_input(void)
{
uint32_t ctrl=0;	

	input_poll_cb();

	for (int q=0;q<8;q++)
	{
		if (input_state_cb(0, RETRO_DEVICE_JOYPAD, 0, buttons[q]))
		{
			ctrl|=1<<(q);
		}
	}

	machine_ctrl(ctrl);

/*
#define RETRO_DEVICE_ID_JOYPAD_B        0
#define RETRO_DEVICE_ID_JOYPAD_Y        1
#define RETRO_DEVICE_ID_JOYPAD_UP       4
#define RETRO_DEVICE_ID_JOYPAD_DOWN     5
#define RETRO_DEVICE_ID_JOYPAD_LEFT     6
#define RETRO_DEVICE_ID_JOYPAD_RIGHT    7
#define RETRO_DEVICE_ID_JOYPAD_A        8
#define RETRO_DEVICE_ID_JOYPAD_X        9

   if (input_state_cb(0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_UP))
//      dir_y--;
   if (input_state_cb(0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_DOWN))
//      dir_y++;
   if (input_state_cb(0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_LEFT))
      //dir_x--;
   if (input_state_cb(0, RETRO_DEVICE_JOYPAD, 0, RETRO_DEVICE_ID_JOYPAD_RIGHT))
      //dir_x++;
*/


}


static void render_update()
{
	/* Try rendering straight into VRAM if we can. */
	struct retro_framebuffer fb = {0};
	uint32_t *buffer;
	uint32_t stride;

	fb.width = SCREEN_W;
	fb.height = SCREEN_H;
	fb.access_flags = RETRO_MEMORY_ACCESS_WRITE;

	if (environ_cb(RETRO_ENVIRONMENT_GET_CURRENT_SOFTWARE_FRAMEBUFFER, &fb) && fb.format == RETRO_PIXEL_FORMAT_XRGB8888)
	{
		buffer = fb.data;
		stride = fb.pitch >> 2;
	}
	else
	{
		buffer = frame_buf;
		stride = SCREEN_W;
	}

	machine_update(buffer,stride);

	video_cb(buffer, SCREEN_W, SCREEN_H, stride << 2);
}

static void check_variables(void)
{
}

static void audio_callback(void)
{
   audio_cb(0, 0);
}


void machine_update();

void retro_run(void)
{

	update_input();
	audio_callback();

	render_update();

	bool updated = false;
	if (environ_cb(RETRO_ENVIRONMENT_GET_VARIABLE_UPDATE, &updated) && updated)
		check_variables();
}

bool retro_load_game(const struct retro_game_info *info)
{
	log_cb(RETRO_LOG_INFO, "Load Game\n");

	enum retro_pixel_format fmt = RETRO_PIXEL_FORMAT_XRGB8888;
	if (!environ_cb(RETRO_ENVIRONMENT_SET_PIXEL_FORMAT, &fmt))
	{
		log_cb(RETRO_LOG_INFO, "XRGB8888 is not supported.\n");
		return false;
	}

	struct retro_keyboard_callback cb = { keyboard_cb };
	environ_cb(RETRO_ENVIRONMENT_SET_KEYBOARD_CALLBACK, &cb);

	memcpy(&ram[0],info->data,info->size);

	machine_init();
	check_variables();

	(void)info;
	return true;
}

void retro_unload_game(void)
{
	log_cb(RETRO_LOG_INFO, "Unload\n");
//	machine_deinit();
}

unsigned retro_get_region(void)
{
   return RETRO_REGION_NTSC;
}

bool retro_load_game_special(unsigned type, const struct retro_game_info *info, size_t num)
{
	log_cb(RETRO_LOG_INFO, "load game special\n");
   if (type != 0x200)
      return false;
   if (num != 2)
      return false;
   return retro_load_game(NULL);
}

//	

#define MACHINE_SIZE (0x40 + (65536*4))

size_t retro_serialize_size(void)
{
   return MACHINE_SIZE;
}

bool retro_serialize(void *data_, size_t size)
{
   if (size < MACHINE_SIZE)
      return false;
	log_cb(RETRO_LOG_INFO, "serialize\n");

	//	save state
	uint8_t *data=(uint8_t*)data_;
	emu_store_state(data);
	memcpy(&data[0x40],&ram[0],65536*4);
	return true;
}

bool retro_unserialize(const void *data_, size_t size)
{
	log_cb(RETRO_LOG_INFO, "unserialize\n");
   if (size < MACHINE_SIZE)
      return false;

	//	load saved state
	uint8_t *data=(uint8_t*)data_;
	emu_load_state(data);
	memcpy(&ram[0],&data[0x40],65536*4);
	return true;
}

void *retro_get_memory_data(unsigned id)
{
   (void)id;
   return NULL;
}

size_t retro_get_memory_size(unsigned id)
{
   (void)id;
   return 0;
}

void retro_cheat_reset(void)
{}

void retro_cheat_set(unsigned index, bool enabled, const char *code)
{
   (void)index;
   (void)enabled;
   (void)code;
}

