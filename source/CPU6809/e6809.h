#ifndef __E6809_H
#define __E6809_H

#include <stdint.h>

/* user defined read and write functions */

uint8_t read8(uint16_t addr);
void write8 (uint16_t address, uint8_t data);

extern unsigned char (*e6809_read8) (unsigned short address);
extern void (*e6809_write8) (unsigned short address, unsigned char data);

unsigned emu_store_state(void *data);
void emu_load_state(void *data);

extern uint32_t cpu_exec (void);
extern void cpu_reset (void);
void cpu_nmi();
void cpu_irq();

#endif