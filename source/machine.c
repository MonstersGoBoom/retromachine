#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#define DISK_ENABLE 1

#if DISK_ENABLE == 1
FILE *disc_fp = NULL;
#endif

#define CPU_SPEED 7.0
#define CPU_CYCLES (CPU_SPEED*1000000)/60
#define SCAN_CYCLES (CPU_CYCLES/SCREEN_H)

// order to emulate such tasks, you should tie them to appropriate number of CPU cycles. For example, if CPU is supposed to run at 2.5MHz and the display uses 50Hz refresh frequency (standard for PAL video), the VBlank interrupt will have to occur every
//      2500000/50 = 50000 CPU cycles
//Now, if we assume that the entire screen (including VBlank) is 256 scanlines tall and 212 of them are actually shown at the display (i.e. other 44 fall into VBlank), we get that your emulation must refresh a scanline each
//       50000/256 ~= 195 CPU cyles
//After that, you should generate a VBlank interrupt and then do nothing until we are done with VBlank, i.e. for
//       (256-212)*50000/256 = 44*50000/256 ~= 8594 CPU cycles
#define CHAR_XSIZE 5
#define CHAR_YSIZE 7

#define CYCLE_SIZE SCREEN_W

#include <math.h>

#include "libretro.h"


#ifdef E6309
#include "e6309.h"
#else 
#include "e6809.h"
#endif

#include "machine.h"

#pragma pack(1)
typedef struct
{
	uint16_t r:4;
	uint16_t a:4;
	uint16_t b:4;
	uint16_t g:4;
} sRGB;

struct 
{
	uint32_t clut[256];	//	converted for display 
	uint32_t bank;			//	bank converted to address offset
	uint8_t *tile_ram;
	uint8_t *tile0_ram;	//	pointer to tiles 
	uint8_t *tile1_ram;	//	pointer to tiles 
	uint8_t *font_ram;	//	pointer to font
	uint8_t irq_line;	
	uint8_t *src;
	uint8_t *dst;
	uint16_t sinc;
	uint16_t dinc;
	uint16_t amount;
	uint16_t steps;
	uint8_t controller;
	uint8_t fgbpp,bgbpp,bpp;
	uint8_t banks[4];
	uint16_t fds_track;
	uint16_t fds_address;
	uint64_t fds_ctrl;
	bool awake;
} VDP;


uint8_t ram[65535*4];	//	max size 
//uint8_t rom[65535*4];	//	max size 

bool clut_update=true;

//	swap 16 bits for endian 
#define SWAP_UINT16(x) ((((x) >> 8) | ((x) << 8))&0xffff)
//	get's the value in console ram as a uint16 
#define PEEK16(x) (((ram[x]) << 8) | ((ram[x+1])))

void machine_ctrl(uint32_t pad)
{
	VDP.controller=pad;
//	printf("%x\n",pad);
}


void POKE16(uint16_t x,uint16_t v)
{
	if ((x>=0xf000) && (x<=0xf200))
		clut_update=true;

	ram[x]=v>>8;
	ram[x+1]=v;
}

//	is the pointer to ram for external use 
#define PVAL16(x) (&ram[VDP.bank+ (((ram[x]) << 8) | (ram[x+1]))])

void printb(char *name,uint32_t b)
{
 	char buffer [33];
	uint32_t pad=0;
  itoa (b,buffer,2);
	pad=32-strlen(buffer);
	printf ("%s:",name);
	for (int q=0;q<pad;q++)
		printf("0");
	printf ("%s\n",buffer);
}

//	read 8 bits from ram 
uint8_t read8 (uint16_t address)
{
	if (address==PPU_SCANLINE)
	{
		return VDP.irq_line;
	}

	if ((address>=0xff30) && (address<=0xff40))
	{
		printf("read %x\n",address);
	}

	if (address==IO_RANDOM)
	{
		return rand();
	}


	if (address==IO_CONTROLLER)
	{
		return VDP.controller;
	}

	return ram[address];
}

uint8_t skip = 0;
uint8_t cmd[5];
uint8_t cursor_x,cursor_y;
uint16_t *screen;
//	write 8 bits
void write8 (uint16_t address, uint8_t data)
{
#if DISK_ENABLE==1
	if (address==IO_FDS_CTRL)
	{
		if (data==1)
		{
			fseek(disc_fp,PEEK16(IO_FDS_TRACK)*256,SEEK_SET);
			fwrite((uint8_t*)&ram[PEEK16(IO_FDS_RAM)],256,1,disc_fp);			
			printf("WRITE\n");
			printf("track %X\n",PEEK16(IO_FDS_TRACK));
			printf("address %X\n",PEEK16(IO_FDS_RAM));
		}
		if (data==0)
		{
			fseek(disc_fp,PEEK16(IO_FDS_TRACK)*256,SEEK_SET);
			fread((uint8_t*)&ram[PEEK16(IO_FDS_RAM)],256,1,disc_fp);			
			printf("READ\n");
			printf("track %X\n",PEEK16(IO_FDS_TRACK));
			printf("address %X\n",PEEK16(IO_FDS_RAM));
		}
	}
#endif

	if (address==PPU_SCANLINE)
	{
		VDP.irq_line = data;	//	don't change the memory
		return;
	}

	if ((address>=0xf000) && (address<0xf200))
		clut_update=true;


	//	writing to 0xff85 triggers the blitter/dma type thing 
	if (address==DMA_TRIGGER)
	{
		ram[address]=data;
		if (VDP.amount>0)
		{
			printf("DMA warning still copying %X %04X %d\n",VDP.amount,address,data);
			cpu_printf();
			return;
		}
		//	trigger copy 
		uint8_t ctrl = ram[PPU_CTRL];
		VDP.bank = (ctrl & 0x3)<<16;
		VDP.src = PVAL16(DMA_SOURCE);
		VDP.dst = (uint8_t*)&ram[PEEK16(DMA_DEST)];
		VDP.sinc = PEEK16(DMA_SRC_INC)<<1;
		VDP.dinc = PEEK16(DMA_DST_INC)<<1;
		VDP.amount = PEEK16(DMA_AMOUNT);
		VDP.steps = VDP.amount/CYCLE_SIZE;
//		printf("%02x:%04x %04x s+%x d+%x a=%x\n",VDP.bank,VDP.src,VDP.dst,VDP.sinc,VDP.dinc,VDP.amount);
	}
	ram[address]=data;
}

uint16_t read16(uint16_t addr)
{
   uint16_t val;

   val = read8(addr) << 8;
   addr++;
   val |= read8(addr);

   return val;
}


uint32_t read32(uint16_t addr)
{
   uint32_t val;

   val  = read8(addr) << 24;
   addr++;
   val |= read8(addr) << 16;
   addr++;
   val |= read8(addr) <<  8;
   addr++;
   val |= read8(addr);
   return val;
}

void vdp_decode_palette()
{
	if (clut_update==true)
	{
		sRGB *srgb=(sRGB*)&ram[PPU_CLUT];
		for (int q=0;q<256;q++)
		{
			VDP.clut[q]=(srgb[q].r<<4)<<16;
			VDP.clut[q]|=(srgb[q].g<<4)<<8;
			VDP.clut[q]|=(srgb[q].b<<4);
		}
		clut_update=false;
	}
}

// old nybble method
void vdp_bg_line(uint16_t tx,uint8_t y,uint16_t xs,uint16_t ys,uint16_t *mram,uint32_t *line)
{
	uint16_t mw = 64;
	for (uint32_t x=tx;x<tx+CYCLE_SIZE;x++)
	{
		//	current x + xscroll
		uint32_t pixelX=x + xs;
		//	current y + yscroll
		uint32_t pixelY=y + ys;
		//	get position in map 
		uint16_t mx = (pixelX>>3)&0x3f;	
		uint16_t my = (pixelY>>3)&0x1f;	
		//	get the tile 
		//	swap the word 
		uint32_t tile = SWAP_UINT16(mram[mx+(my*mw)]);	//	grab the 16 bit value
		//uint32_t tile = mram[mx+(my*mw)];	//	grab the 16 bit value
		//	clut offset 4 bits = 16 palettes 
		uint8_t cluto = ((tile>>12)&15)<<4;							//	color offset

		//	xflip 
		uint8_t xflip = (tile >> 11)&1;									//	xflip
		//	mask bits 
		tile &= 0x3ff;
		//	if it's 0 we skip it 
		if (tile!=0)
		{
			uint8_t byte = 0;
			if (xflip==0)
			{
				byte = VDP.tile_ram[(tile<<5) + (((pixelX>>1)&3) + ((pixelY&7)*4))];
				if ((pixelX&1)==0)	byte=byte>>4;
			}
			else
			{
				byte = VDP.tile_ram[(tile<<5) + (((3-(pixelX>>1))&3) + ((pixelY&7)*4))];
				if ((pixelX&1)==1)	byte=byte>>4;
			}
//			cluto = 0;
			//	draw if not index 0 
			if ((byte&0xf)!=0)
				line[x]=VDP.clut[cluto+(byte&0xf)];
		}		
	}	
}

void vdp_bg_line_planar(uint16_t tx,uint8_t y,uint16_t xs,uint16_t ys,uint16_t *mram,uint32_t *line)
{
	uint16_t mw = 64;
	for (uint32_t x=tx;x<tx+CYCLE_SIZE;x++)
	{
		//	current x + xscroll
		uint32_t pixelX=x + xs;
		//	current y + yscroll
		uint32_t pixelY=y + ys;
		//	get position in map 
		uint16_t mx = (pixelX>>3)&0x3f;	
		uint16_t my = (pixelY>>3)&0x1f;	
		//	get the tile 
		//	swap the word 
		uint32_t tile = SWAP_UINT16(mram[mx+(my*mw)]);	//	grab the 16 bit value
		//	clut offset 4 bits = 16 palettes 
		uint8_t cluto = ((tile>>12)&15)<<4;							//	color offset
		//	xflip 
		uint8_t xflip = (tile >> 11)&1;									//	xflip
		tile &= 0x3ff;
		//	if it's 0 we skip it 
		if (tile!=0)
		{
			uint8_t *bytes = &VDP.tile_ram[(tile<<5) + ((pixelY&7))];
			if (xflip==0)
				pixelX=7-(pixelX&7);
			else
				pixelX=(pixelX&7);
			
			uint8_t byte = (bytes[0] >> pixelX)&1;
			if (VDP.bpp>=1)
			{
				byte |= ((bytes[8] >> pixelX)&1)<<1;
				if (VDP.bpp>=2)
				{
					byte |= ((bytes[16] >> pixelX)&1)<<2;
					if (VDP.bpp>=3)
					{
						byte |= ((bytes[24] >> pixelX)&1)<<3;
					}
				}
			}

			if (byte!=0)
				line[x]=VDP.clut[cluto+byte];
		}		
	}	
}

void vdp_bg_line_1bpp6(uint16_t tx,uint8_t y,uint16_t xs,uint16_t ys,uint16_t *mram,uint32_t *line)
{
	uint16_t mw = 64;
	for (uint32_t x=tx;x<tx+CYCLE_SIZE;x++)
	{
		//	current x + xscroll
		uint32_t pixelX=x + xs;
		//	current y + yscroll
		uint32_t pixelY=y + ys;
		//	get position in map 
		uint16_t mx = (pixelX/CHAR_XSIZE)&0x3f;	
		uint16_t my = (pixelY/CHAR_YSIZE)&0x1f;	
		//	get the tile 
		//	swap the word 
		uint32_t tile = mram[mx+(my*mw)];	//	grab the 16 bit value
		//uint32_t tile = mram[mx+(my*mw)];	//	grab the 16 bit value
		//	clut offset 4 bits = 16 palettes 
		uint8_t cluto = ((tile>>12)&15);							//	color offset

		//	xflip 
//		uint8_t xflip = (tile >> 15)&1;									//	xflip
		//	mask bits 
		tile &= 0x3ff;
		//	if it's 0 we skip it 
		if (tile!=0)
		{
			uint8_t *bytes = &VDP.tile_ram[(tile<<3) + ((pixelY%CHAR_YSIZE))];
			pixelX=7-(pixelX%CHAR_XSIZE);
			
			uint8_t byte = (bytes[0] >> pixelX)&1;
			if (byte!=0)
				line[x]=VDP.clut[cluto];
		}		
	}	
}

void vdp_txt_line(uint16_t tx,uint8_t y,uint16_t xs,uint16_t ys,uint8_t *mram,uint32_t *line)
{
	uint16_t mw = 64;
	for (uint32_t x=tx;x<tx+CYCLE_SIZE;x++)
	{
		//	current x + xscroll
		uint32_t pixelX=x + xs;
		//	current y + yscroll
		uint32_t pixelY=y ;
		//	get position in map 
		uint16_t mx = (pixelX/6)&0x3f;	
		uint16_t my = (pixelY>>3)&0x1f;	
		//	get the tile 
		//	swap the word 
		uint8_t tile = mram[mx+(my*mw)]-0x1f;	//	grab the 16 bit value
		//	if it's 0 we skip it 
		if (tile!=0)
		{
			uint8_t byte = VDP.font_ram[(tile<<3) + (pixelY&7)];
			pixelX=7-(pixelX%6);
			byte = (byte >> pixelX)&1;
			//	draw if not index 0 
			if (byte!=0)
				line[x]=VDP.clut[1];
		}		
	}	
}

void vdp_spr_line(uint8_t y,uint32_t *line)
{
	OAM *oam=(OAM*)&ram[PPU_SPRRAM];
	for (int q=0;q<128;q++)
	{
		attribs *bits;
		uint16_t data;
		data = SWAP_UINT16(oam->data);
		bits=(attribs*)&data;
		int16_t ox = SWAP_UINT16(oam->x)-128;
		if (bits->tile!=0)
		{
			uint8_t swidth=(bits->xsize+1)<<3;
			uint8_t sheight=(bits->ysize+1)<<3;
			uint8_t shmod = sheight-1;
			uint8_t cluto = oam->flags&0xf0;							//	color offset
			if (y >= oam->y  && y < oam->y+sheight)
			{
				int _y = (y-oam->y) & shmod;
				for (int x=0;x<swidth;x++)
				{
					int tile = bits->tile + (x>>3) + ((_y>>3)*(bits->xsize+1));
					uint8_t *bytes = &VDP.tile_ram[(tile<<5) + ((_y&7))];
					int pixelX=7-(x&7);
					uint8_t bpp=oam->flags&3;
					uint8_t byte = (bytes[0] >> pixelX)&1;
					if (bpp>=1)
					{
						byte |= ((bytes[8] >> pixelX)&1)<<1;
						if (bpp>=2)
						{
							byte |= ((bytes[16] >> pixelX)&1)<<2;
							if (bpp>=3)
							{
								byte |= ((bytes[24] >> pixelX)&1)<<3;
							}
						}
					}
					if (byte!=0)
						if ((ox+x>0) && (ox+x<SCREEN_W))
							line[ox+x]=VDP.clut[cluto+byte];
				}
			}
		}
		oam++;
	}
}

void (*vdp_bg_line_bpp)(uint16_t tx,uint8_t y,uint16_t xs,uint16_t ys,uint16_t *mram,uint32_t *line);
void (*vdp_bg_line_bpp1)(uint16_t tx,uint8_t y,uint16_t xs,uint16_t ys,uint16_t *mram,uint32_t *line);

uint16_t vdp_step(int x,int y,uint32_t *line)
{
	ram[PPU_SCANLINE] = (uint8_t)y;


	int dma_eat=0;

	if (VDP.amount>0)
	{
//		printf("d %x s %x %x\n",&VDP.dst[0]-&ram[0],&VDP.src[0]-&ram[0],VDP.amount);
	}

	for (int q=0;q<CYCLE_SIZE;q++)
	{
		if (VDP.amount>0)
		{
			VDP.dst[0] = VDP.src[0];
			VDP.dst[1] = VDP.src[1];
			VDP.src+=VDP.sinc;
			VDP.dst+=VDP.dinc;
			VDP.amount--;
			POKE16(DMA_AMOUNT,VDP.amount);
			dma_eat++;
		}				
	}
	//	if we ate dma then don't render
	if (dma_eat!=0) return CYCLE_SIZE;

	if (y<SCREEN_H)
	{
		x=x*CYCLE_SIZE;

		vdp_decode_palette();

		uint8_t ctrl = ram[PPU_CTRL];

		VDP.bank = (ctrl &0x3)<<16;
		//	set bg of current slice to CLUT 0 
		for (int qx=x;qx<x+CYCLE_SIZE;qx++)
			line[qx]=VDP.clut[0];

		//	get address of tiles
		VDP.tile0_ram=PVAL16(PPU_TILES0);
		VDP.tile1_ram=PVAL16(PPU_TILES1);
		//	map 1 enabled and txt mode is off

		VDP.tile_ram = VDP.tile1_ram;		
		VDP.bpp = VDP.bgbpp;
		if ((ctrl&PPU_MAP1_ENABLE)!=0)
			vdp_bg_line_planar(x,y,PEEK16(PPU_MAP1_X),ram[PPU_MAP1_Y],(uint16_t*)&ram[PPU_MAP1],line);

		VDP.tile_ram = VDP.tile0_ram;		
		VDP.bpp = VDP.fgbpp;
		//	write MAP0 ( infront )
		if ((ctrl&PPU_MAP0_ENABLE)!=0)
			vdp_bg_line_planar(x,y,PEEK16(PPU_MAP0_X),ram[PPU_MAP0_Y],(uint16_t*)&ram[PPU_MAP0],line);
		if ((ctrl&PPU_SPR_ENABLE)!=0)
			vdp_spr_line(y,line);


		//	map 1 enabled and txt mode is on
//		if ( ((ctrl&PPU_MAP1_ENABLE)!=0) && (ctrl&PPU_MAP1_TXT)!=0)
//			vdp_txt_line(x,y,PEEK16(PPU_MAP1_X),ram[PPU_MAP1_Y],(uint8_t*)&ram[PPU_MAP1],line);

		return 1;
	}
	return 0;
}


void disc_init()
{
#if DISK_ENABLE==1
	int len;
	if (disc_fp==NULL)
	{
		disc_fp = fopen("disc.00","r+");
		if (disc_fp==NULL)
		{
			disc_fp = fopen("disc.00","w+");
			uint8_t track[256];
			printf("create disc\n");
			memset(&track[0],0,256);
			for (int q=0;q<256;q++)
			{
				fwrite(track,256,1,disc_fp);
			}
			fseek(disc_fp,0,SEEK_SET);
		}
	}
#endif
}

void machine_deinit()
{
	VDP.awake = false;
#if DISK_ENABLE==1
	printf("close\n");
	if (disc_fp!=NULL)
		fclose(disc_fp);
#endif
	FILE *fp=fopen("state.bin","wb");
	fwrite(&ram[0],65536,4,fp);
	fclose(fp);
}

void machine_init()
{
	VDP.awake = true;
	cursor_x=cursor_y=0;
	//	force IRQ line to bottom 
	VDP.irq_line=1;
	
//	memcpy(rom,ram,65536*4);

	int size = 0xff00 - 0xf200;

	printf("%x size %d sprites\n",size,size/sizeof(OAM));
	printf("%f %f %f\n",CPU_SPEED,CPU_CYCLES,SCAN_CYCLES);
	cpu_reset();
	disc_init();

	uint16_t *map0=(uint16_t*)&ram[PPU_MAP0];
	uint16_t *map1=(uint16_t*)&ram[PPU_MAP1];
	//	fill vram with garbage 
	for (int y=0;y<64*32;y++)
	{
		map0[y]=y&0xef;
		map1[y]=y&0x3f;
	}
}

#define RAMVAL(x) (VDP.bank+ (((ram[x]) << 8) | (ram[x+1])))

void ppu_log()
{
	printf("PPU\n");
	printf("Bank %X %X %X\n",VDP.bank,PPU_CTRL,ram[PPU_CTRL]);
	printf("PPU_TILES0 %08x PPU_TILES1 %08x\n",RAMVAL(PPU_TILES0),RAMVAL(PPU_TILES1));
}

//	machine specific here 
void machine_update(uint32_t *buffer,uint32_t stride)
{
//	FILE *fp=fopen("memory.bin","wb");
//	fwrite(&ram[0],65536,1,fp);
//	fclose(fp);
//	ppu_log();
	if (VDP.awake==false) return;
	uint32_t cycles=0;
	for (int y=0;y<256;y++)
	{
		ram[PPU_SCANLINE]=y;
		if ((y==VDP.irq_line) && (y!=255))
		{
			cpu_irq();
		}
		if (y==255)
		{
			cpu_nmi();
		}

		for (int x=0;x<SCAN_CYCLES;x++)
			cycles+=cpu_exec();

		if ((y&0x7)==0)
		{
			uint8_t mode = ram[PPU_BPP_MODE];
			VDP.fgbpp = mode&3;
			VDP.bgbpp = (mode>>4)&3;
		}
		uint32_t *line = buffer + (y*stride);
		vdp_step(0,y,line);
	} 

/*
	OAM *oam=(OAM*)&ram[PPU_SPRRAM];
	oam->x = 32;
	oam->y = 32;
	oam->xsize = 0;
	oam->ysize = 3;
	oam->tile = 1;
	oam++;

	oam->x = 42;
	oam->y = 32;
	oam->xsize = 1;
	oam->ysize = 0;
	oam->tile = 1;
	oam->clut = 1;
	oam++;

	oam->x = 62;
	oam->y = 32;
	oam->xsize = 2;
	oam->ysize = 3;
	oam->tile = 1;
	oam->clut = 2;
	oam++;

	oam->x = 92;
	oam->y = 32;
	oam->xsize = 3;
	oam->ysize = 3;
	oam->tile = 1;
	oam->clut = 3;

	oam++;
*/
	//printb("Controller",VDP.controller);

	//printf("%d cycles\n",cycles);
}
