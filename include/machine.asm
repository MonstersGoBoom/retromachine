; 
PPU_MAP0_ON				EQU 	1<<4
PPU_MAP1_ON 			EQU 	1<<5
PPU_SPR_ON 				EQU 	1<<6
PPU_MAP1_TXT			EQU 	1<<7

MAP0							EQU 	$d000
MAP1							EQU 	$e000
CLUT							EQU 	$f000
SPRRAM						EQU 	$f200

IO_SELECT					EQU 1<<0
IO_START					EQU 1<<1
IO_UP							EQU 1<<2
IO_DOWN						EQU 1<<3
IO_LEFT						EQU	1<<4
IO_RIGHT					EQU 1<<5
IO_A							EQU 1<<6
IO_B							EQU 1<<7

PPU_TILES0 				EQU		$ff00 
PPU_TILES1			 	EQU		$ff02 
PPU_MAP0_X 				EQU		$ff04
PPU_MAP1_X 				EQU		$ff06
PPU_MAP0_Y 				EQU		$ff08
PPU_MAP1_Y 				EQU 	$ff09
PPU_SCANLINE 			EQU		$ff12

PPU_CTRL					EQU		$ff13

PPU_BPP_MODE	    EQU		$ff14

IO_CONTROLLER			EQU 	$ff20; 	controller 

DMA_SOURCE				EQU 	$ff80; copy from here (include bank )
DMA_DEST					EQU 	$ff82; to ram 
DMA_AMOUNT				EQU 	$ff84; 	this write will trigger the copy
DMA_SRC_INC				EQU		$ff86; 	source increment
DMA_DST_INC				EQU		$ff88; 	dest increment

