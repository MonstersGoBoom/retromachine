#ifndef _MACHINE_H_
#define _MACHINE_H_

#define SCREEN_W 320
#define SCREEN_H 200

#ifdef RETROMACHINE
#define RAM(addr, type) (addr)
#define ROM(addr, type) (addr)
#else 
typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef signed short int16_t;
typedef unsigned short uint16_t;
#define RAM(addr, type) *((volatile type* const) addr)
#define ROM(addr, type) *((const type* const) addr)

#define XSIZE8 0
#define XSIZE16 1<<11
#define XSIZE24 2<<11
#define XSIZE32 3<<11
#define YSIZE8 0
#define YSIZE16 1<<13
#define YSIZE24 2<<13
#define YSIZE32 3<<13

#endif 

#define PPU_TILES0 RAM(0xff00,uint16_t)
#define PPU_TILES1 RAM(0xff02,uint16_t)

#define PPU_MAP0_X RAM(0xff04,uint16_t)
#define PPU_MAP1_X RAM(0xff06,uint16_t)

#define PPU_MAP0_Y RAM(0xff08,uint8_t)
#define PPU_MAP1_Y RAM(0xff09,uint8_t)

//	FF10,FF11 unused

#define PPU_SCANLINE 	RAM(0xff12,uint8_t)

#define PPU_MAP0_ON		1<<4
#define PPU_MAP1_ON 	1<<5
#define PPU_SPR_ON 		1<<6
#define PPU_MAP1_TXT	1<<7

#define PPU_CTRL 			RAM(0xff13,uint8_t)
#define PPU_BPP_MODE	RAM(0xff14,uint8_t)

//	FF15-FF1f unused

#define PPU_MAP0_ENABLE 1<<4
#define PPU_MAP1_ENABLE 1<<5
#define PPU_SPR_ENABLE 	1<<6
#define PPU_MAP1_TXT 		1<<7

#define PPU_MAP0	 0xd000//; 64x32 words
#define PPU_MAP1	 0xe000//; 64x32 words
#define PPU_CLUT	 0xf000//; 256 colors 4444
#define PPU_SPRRAM 0xf200//; 

//	FF20 IO

#define IO_SELECT 1<<0
#define IO_START 	1<<1
#define IO_UP 		1<<2
#define IO_DOWN 	1<<3
#define IO_LEFT 	1<<4
#define IO_RIGHT 	1<<5
#define IO_A 			1<<6
#define IO_B 			1<<7

#define IO_CONTROLLER RAM(0xff20,uint8_t)
#define IO_RANDOM			RAM(0xff21,uint8_t)

#define IO_FDS_CTRL 	RAM(0xff22,uint8_t)
#define IO_FDS_TRACK 	RAM(0xff24,uint16_t)
#define IO_FDS_RAM 		RAM(0xff26,uint16_t)
#define IO_FDS_END		RAM(0xff28,uint8_t)

// COPY CHIP FF80 Copy Chip
#define DMA_SOURCE	RAM(0xff80,uint16_t)	// copy from here ( include bank )
#define DMA_DEST		RAM(0xff82,uint16_t)  // to ram 
#define DMA_AMOUNT	RAM(0xff84,uint16_t)	//	this write will trigger the copy
#define DMA_TRIGGER	RAM(0xff85,uint8_t)	//	this write will trigger the copy
#define DMA_SRC_INC	RAM(0xff86,uint16_t)	//	source increment
#define DMA_DST_INC	RAM(0xff88,uint16_t)	//	dest increment

//#define PAGES				RAM(0xff90,uint16_t)	//	dest increment

typedef struct 
{
	uint16_t tile:11;			
	uint16_t xsize:2;			//	8,16,24,32
	uint16_t ysize:2;			//	8,16,24,32
	uint16_t spare:1;
} attribs;

//	40 * 6 0xf0 bytes   
//	6 bytes per sprite 
typedef struct 
{
	//	word 2
	uint16_t x;				
	//	word 2
	uint16_t data; 
	//	byte 1
	uint8_t flags;	//	bpp , flip x,y, clut
	//	byte 1
	uint8_t y;				
} OAM;

#endif


