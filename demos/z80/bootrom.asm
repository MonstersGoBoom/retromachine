	.include "machine.zasm"
	.bank prog
	.area .text  (BANK=prog)
	.area	_CODE(ABS)
	.org 0x0000
	jp START 


	.org 0x0010
	.word 0
	.word 0 

	.org 0x0038
	;	pull address from 0x10-0x11 call that for IRQ
IRQ:
	ld a,(0x10);
	ld l,a
	ld a,(0x11)
	ld h,a 
	jp (hl)

	;	pull address from 0x12-0x13 call that for NMI
	.org 0x0066
NMI:
	ld a,(0x12);
	ld l,a
	ld a,(0x13)
	ld h,a 
	jp (hl)

START:
	di 
	;	set IRQ pointer
	ld a, #<IRQhandle
	ld (0x10),a
	ld a, #>IRQhandle
	ld (0x11),a
	; set NMI pointer
	ld a, #<NMIhandle
	ld (0x12),a
	ld a, #>NMIhandle
	ld (0x13),a

	;	set 4bpp for map0
	ld a ,#30
	ld (PPU_BPP_MODE),a

	; set bank 1 and mode enable

	ld a ,#1<<4 | 1
	ld (PPU_CTRL),a

	;	stack pointer
	ld sp, MAP0
	ei  
	im 1       
DONE:	
	halt 
	jp DONE

NMIhandle:
	;	screen off but stay in bank
	ld a , 1
	ld (PPU_CTRL),a

	;	bounce the scan stop point around

	ld  a,(scandir)
	ld  b,a
	ld 	a,(scanpos)
	ld 	(PPU_SCANLINE),a
	add a,b
	ld 	(scanpos),a
	cp 	a,#224
	jp 	nz,notbottom
	ld a,-1
	ld (scandir),a
	jp nottop
notbottom:	
	cp 	a,#0
	jp 	nz,nottop
	ld a,1
	ld (scandir),a
nottop:

	;	set clut to 0 
	ld 	a, #0
	ld 	(CLUT+1),a

	;	reset IRQ pointer to first handler 
	ld a, #<IRQhandleLine0
	ld (0x10),a
	ld a, #>IRQhandleLine0
	ld (0x11),a

	ei

	retn

IRQhandle:

	ld	a,(CLUT+1)
	add a,#11
	ld 	(CLUT+1),a

	ld 	a,(PPU_SCANLINE)
	add a,4
	ld 	(PPU_SCANLINE),a
	ei
	reti

IRQhandleLine0:
	di
	ld a, #<IRQhandle
	ld (0x10),a
	ld a, #>IRQhandle
	ld (0x11),a
	
	ld 	a,(PPU_SCANLINE)
	ld 	b,a
	add a,4
	ld 	(PPU_SCANLINE),a

	ld a,#224
	sub a,b 
	ld (PPU_MAP0_Y),a

	ld a ,#1<<4 | 1
	ld (PPU_CTRL),a

	ei
	ret

	.area	.data


scanpos: .byte 0
scandir: .byte 1



