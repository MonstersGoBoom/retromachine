#include "machine.h"

uint16_t *PPU_CLUT_PTR=(uint16_t*)PPU_CLUT;	
uint16_t *PPU_MAP0_PTR=(uint16_t*)PPU_MAP0;	
uint16_t *PPU_MAP1_PTR=(uint16_t*)PPU_MAP1;	

uint16_t irq_pos=224<<8;
uint16_t irq_move=-1;
uint16_t id=0;

OAM *oam=(OAM*)PPU_SPRRAM;

void nmi_cb()
{
	PPU_SCANLINE = irq_pos>>8;
	PPU_CLUT_PTR[0]=0x0000;
	id = 0;	
	PPU_MAP0_X+=1;
	PPU_CTRL=1;
	irq_move-=1<<4;
	irq_pos+=irq_move;
	if (irq_pos<1024)
		irq_move=-(irq_move+32);

	PPU_SCANLINE = 1;
	uint8_t pad = IO_CONTROLLER;
	if ((pad&IO_UP)!=0) oam[0].y--;
	if ((pad&IO_DOWN)!=0) oam[0].y++;
	if ((pad&IO_LEFT)!=0) oam[0].x--;
	if ((pad&IO_RIGHT)!=0) oam[0].x++;

}

void irq_cb()
{
	if (id==0)
	{
		PPU_MAP0_Y=-PPU_SCANLINE;
		id=1;
	}
	PPU_SCANLINE+=1;
	PPU_CTRL=PPU_MAP0_ON|PPU_MAP1_ON|PPU_SPR_ON|1;
	PPU_CLUT_PTR[0]+=0x0004;
}


void setup()
{
	int q,x,y;
	q=0;
	for (y=0;y<32;y++)
	{
		for (x=0;x<64;x++)
		{
			PPU_MAP0_PTR[x+(y*64)]=(q&0x7ff);
			PPU_MAP1_PTR[x+(y*64)]=(q&0x7ff);
			q++;
		}
	}
	for (y=0;y<128;y++)
	{
		oam[y].x = 128+IO_RANDOM;
		oam[y].y = IO_RANDOM;
		oam[y].flags = (y&3) | ((y&0xf)<<4);
		oam[y].data = IO_RANDOM | XSIZE8 | YSIZE8;
	}
	oam[0].x = 128;
	oam[0].y = 128;
	oam[0].flags = 3;	
	oam[0].data = 512 | XSIZE32 | YSIZE32;
}

int main()
{
	
	PPU_TILES0=0;
	PPU_TILES1=0;
	PPU_BPP_MODE=0x13;
	setup();

	int x;
	for (x=0;x<8;x++)
	{
		IO_FDS_RAM = &PPU_MAP0_PTR[(x * 256)];
	//	set track
		IO_FDS_TRACK = x;
	//	write 
		IO_FDS_CTRL=1;
	}
	//	set ram address

	//	read block
	IO_FDS_RAM = 0x800;
	IO_FDS_TRACK=0;
	IO_FDS_CTRL = 0;
	
	while (1)
	{
	}
	return 0;
}




