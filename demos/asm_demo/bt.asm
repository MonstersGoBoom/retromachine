	include "../../include/machine.asm"

MAPW EQU $100

	ORG $000
RESET:
	ORCC #$FF		; Disable interrupts
	; set our stack
	LDS #$d000

	lda #$30
	sta PPU_BPP_MODE

	;jsr clear_vram
	;
	; display off bank 1
	;
	lda #$01	
	sta PPU_CTRL; 	from ROM 

	jsr COPY_MAP

	;	copy palette
	ldx #bt_pal
	stx DMA_SOURCE; to RAM CLUT 
	ldx #CLUT 
	stx DMA_DEST; increment by words
	ldx #1
	stx DMA_SRC_INC
	stx DMA_DST_INC; 	copy 256 words
	ldx #$100
	stx DMA_AMOUNT; 	done

	jsr SYNC_DMA
	
	; set tile address to rom address 
	ldx #bt_cel
	stx PPU_TILES1

	ldx #font
	stx PPU_TILES0
	
	; 	enable map 0 and sprites and set bank to 1 	

	lda #PPU_MAP0_ON | PPU_MAP1_ON | PPU_SPR_ON | 1
	sta PPU_CTRL

;	ANDCC #$E3  
	ANDCC #10111111

	CLR	CFLBA3
	CLR	CFLBA2
	CLR	CFLBA1
	CLR	CFLBA0
	JSR	CFINIT
;;	JSR	CFINFO


	; sit and wait until scanline at $c0	
STALL:
	jmp STALL

;	NMI routine fires at end of frame
;	just scroll and set initial scanline of IRQ 
NMI:
	ldx #$0000
	stx CLUT
	lda #$20
	sta PPU_SCANLINE

	lda #PPU_MAP0_ON | PPU_MAP1_ON | PPU_SPR_ON | 1
	sta PPU_CTRL

	ldx xscroll
	inx
	stx xscroll

	;	copy smooth scroll
	ldx xscroll
	stx PPU_MAP1_X

	;	check if we need to update edge
	clc 
	lda xscroll+1
	anda #$7 
	cmpa #$0
	bne scroll

	inc xmap
	lda #PPU_MAP0_ON | PPU_MAP1_ON | PPU_SPR_ON | 2
	sta PPU_CTRL

	; copy one line from map to screen; 	B is xmap pos
	;	xmap is WORD location to copy from 
	ldb xmap
	; source increment is 256 words ( or map width ) 
	ldx #MAPW
	stx DMA_SRC_INC
	; destination increment is 64 words ( screen map width )
	ldx #$40 
	stx DMA_DST_INC

	;	address of map ( rom address )
	ldx #bt_map+(97*512)
	;	add xmap
	abx 
	abx 
	;	set source copy
	stx DMA_SOURCE
	
	; set screen address, screen + x	
	clc
	ldx #MAP1
	;	offscreen ( if we do 0 then we see the blocks on screen )
	sbcb #4
	;	wrap around 
	abx 
	abx 
	andb #$3f
	stx DMA_DEST 
	
	;	copy $18 lines
	ldx #28
	stx DMA_AMOUNT

	lda #PPU_MAP0_ON | PPU_MAP1_ON | PPU_SPR_ON | 1
	sta PPU_CTRL
scroll:	

	rti

;	IRQ is called when scanline = PPU_SCANLINE
; read is actualy scanline 
; write is next irq line 
IRQ:	

	lda PPU_SCANLINE
	;	read current , add 16 , then the next irq is 16 pixels down from us 
	adda #$2
	sta PPU_SCANLINE
	clc
	lda IO_CONTROLLER 
	bita #IO_RIGHT
	bne skipi

;;	jmp $d000

	ldx CLUT
	inx
	stx CLUT
skipi:	
	rti

;	simply wait for the DMA to finish 
SYNC_DMA:
	ldx DMA_AMOUNT
	cmpx #0
	bne SYNC_DMA
	rts

;	use a word to clear the VRAM 
clear_word:fcw 0

clear_vram:
	lda #$00
	sta PPU_CTRL; 	point to our clear word
	ldx #clear_word
	stx DMA_SOURCE
	ldx #MAP0
	stx DMA_DEST
	;	don't increment the source 
	;	keep at clearword 
	ldx #$00
	stx DMA_SRC_INC 
	; inc dest by 1 word
	ldx #$01
	stx DMA_DST_INC
	ldx #$1100
	stx DMA_AMOUNT
	jsr SYNC_DMA
	rts

COPY_MAP:	
;	ldx #bt_map+(22*512)
;	stx DMA_SOURCE
;	ldx #MAP1
;	stx DMA_DEST
;	ldx #$01
;	stx DMA_SRC_INC 
;	stx DMA_DST_INC
;	ldx #64*28
;	stx DMA_AMOUNT
;	jsr SYNC_DMA

	ldx #string
	ldy #MAP0
	lda #$8
	sta counter
 	ldb #$00
loop_start:           
	clc


	tfr b,a
	clc
	anda #$f
	asla 
	asla 
	asla 
	asla 
	anda #$f0
	sta ,y+

	lda ,x+
	suba #$20
	sta ,y+

	decb 
  bne loop_start  	
	clc
	lda counter
	deca 
	sta counter
	cmpa #0
	bne loop_start
	rts

counter:	fcb 0

FCB	$04
CFLBA3	RMB	1
CFLBA2	RMB	1
CFLBA1	RMB	1
CFLBA0	RMB	1

CFBASE	EQU 	$ff30
CFREG0	EQU	CFBASE+0	DATA PORT
CFREG1	EQU	CFBASE+1	READ: ERROR CODE, WRITE: FEATURE
CFREG2	EQU	CFBASE+2	NUMBER OF SECTORS TO TRANSFER
CFREG3	EQU	CFBASE+3	SECTOR ADDRESS LBA 0 [0:7]
CFREG4	EQU	CFBASE+4	SECTOR ADDRESS LBA 1 [8:15]
CFREG5	EQU	CFBASE+5	SECTOR ADDRESS LBA 2 [16:23]
CFREG6	EQU	CFBASE+6	SECTOR ADDRESS LBA 3 [24:27 (LSB)]
CFREG7	EQU	CFBASE+7	READ: STATUS, WRITE: COMMAND

;********************************
;* INITIALIZE CF
;********************************
CFINIT:
	LDA	#$04	;	RESET COMMAND
	STA	CFREG7
	JSR	CFWAIT
	LDA	#$E0	;	LBA3=0, MASTER, MODE=LBA
	STA	CFREG6
	LDA	#$01	;	8-BIT TRANSFERS
	STA	CFREG1
	LDA	#$EF	;	SET FEATURE COMMAND
	STA	CFREG7
	JSR	CFWAIT
	JSR	CFCHERR
	RTS

;********************************
;* WAIT FOR CF READY
;********************************
CFWAIT:	
	LDA	CFREG7
	ANDA	#$80	;	MASK OUT BUSY FLAG
	CMPA	#$00
	BNE	CFWAIT
	RTS

;********************************
;* CHECK FOR CF ERROR
;********************************
CFCHERR:	LDA	CFREG7
	ANDA	#$01;		MASK OUT ERROR BIT
	CMPA	#0
	BEQ	CFNERR
	LDA	#'!
	sta $ff21
;	JSR	OUTCHAR
	LDX	CFREG1
	sta $ff21
;	JSR	OUT2HS
CFNERR:	RTS

********************************
* READ DATA FROM CF
********************************
CFREAD:	
	JSR	CFWAIT
	LDA	CFREG7
	ANDA	#%00001000	;FILTER OUT DRQ
	CMPA	#%00001000
	BNE	CFREADE
	LDA	CFREG0	;	READ DATA BYTE
	STA	,X
	INX
	BRA	CFREAD
CFREADE	RTS

;********************************
;* CF SET LBA
;********************************
CFSLBA:
	LDA	CFLBA0	;	LBA 0
	STA	CFREG3
	LDA	CFLBA1	;	LBA 1
	STA	CFREG4
	LDA	CFLBA2	;	LBA 2
	STA	CFREG5	
	LDA	CFLBA3	;	LBA 3
	ANDA	#%00001111	;FILTER OUT LBA BITS
	ORA	#%11100000	;MODE LBA, MASTER DEV
	STA	CFREG6
	RTS

BLKDAT	
	RMB	512

string: 
	fcc "A screen of Text                                                "
	fcc "------------------------------------------------------          "
	fcc "Lorem ipsum dolor sit amet, consectetur adipiscing el           "
	fcc "it, sed do eiusmod tempor incididunt ut labore et               "
	fcc "dolore magna aliqua. Ut enim ad minim veniam, quis              "
	fcc "nostrud exercitation ullamco laboris nisi ut aliquip            "
	fcc "ex ea commodo consequat. Duis aute irure dolor in               "
	fcc "reprehenderit in voluptate velit esse cillum dolore             "
	fcc "eu fugiat nulla pariatur. Excepteur sint occaecat               "
	fcc "cupidatat non proident, sunt in culpa qui officia               "
	fcc "deserunt mollit anim id est laborum.                            "
	fcc "                                                                "
	fcc "Lorem ipsum dolor sit amet, consectetur adipiscing el           "
	fcc "it, sed do eiusmod tempor incididunt ut labore et               "
	fcc "dolore magna aliqua. Ut enim ad minim veniam, quis              "
	fcc "nostrud exercitation ullamco laboris nisi ut aliquip            "
	fcc "ex ea commodo consequat. Duis aute irure dolor in               "
	fcc "reprehenderit in voluptate velit esse cillum dolore             "
	fcc "eu fugiat nulla pariatur. Excepteur sint occaecat               "
	fcc "cupidatat non proident, sunt in culpa qui officia               "
	fcc "deserunt mollit anim id est laborum.                            "
	fcc "                                                               k"
	fcc "------------------------------------------------------         j"
	fcc "0123456789-=\[];'{}:,.<>/?ABCDEFGHIJKLMNOPQRSTUVWXYZ           i"
	fcc "0123456789-=\[];'{}:,.<>/?abcdefghijklmnopqrstuvwxyz           h"
	fcc "                                                               g"
	fcc "0123456789-=\[];'{}:,.<>/?ABCDEFGHIJKLMNOPQRSTUVWXYZ           f"
	fcc "0123456789-=\[];'{}:,.<>/?abcdefghijklmnopqrstuvwxyz           e"
	fcc "0123456789-=\[];'{}:,.<>/?ABCDEFGHIJKLMNOPQRSTUVWXYZ           d"
	fcc "0123456789-=\[];'{}:,.<>/?abcdefghijklmnopqrstuvwxyz           c"
	fcc "------------------------------------------------------         b"
	fcc "A screen of Text                                               a"


xscroll:	fcw 0
xmap:			fcw -32 

	ORG $fffa
	fcw IRQ
	fcw NMI
  fcw RESET

;;	bank 
	ORG $10000

font: 
	bin "../assets/font1bit_tiles.bin"

bt_cel:
  bin "../assets/blacktiger_tiles.bin" ; sprites.chr should be 8192 bytes

bt_pal:
	bin "../assets/blacktiger_clut.bin"
	;	using another bank just for map data 
	ORG $20000
bt_map:
  bin  "../assets/blacktiger_map.bin" 







	