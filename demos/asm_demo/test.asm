	.module	bootrom.asm
	.area .text
	.globl _RESET

.include "include/machine.asm"
.org $000
RESET:
	; set our stack
	LDS #$cfff
STALL	
	jmp STALL

;	NMI routine fires at end of frame
;	just scroll and set initial scanline of IRQ 
NMI:
	ldx #$0000
	stx CLUT
	lda #$20
	sta PPU_SCANLINE
	rti

;	IRQ is called when scanline = PPU_SCANLINE
; read is actualy scanline 
; write is next irq line 
IRQ:	
	lda PPU_SCANLINE
	adda #$1
	sta PPU_SCANLINE
	ldx CLUT
	inx
	stx CLUT
	rti

	ORG $fffa
	fcw IRQ
	fcw NMI
  fcw RESET

;;	bank 
	ORG $10000

surt_pal:
	BIN "surt_pal.bin"

surt_cel:
  BIN "surt_cel_planar.bin" ; sprites.chr should be 8192 bytes

surt_map:
  BIN "surt_map.bin" 







	