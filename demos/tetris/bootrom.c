//	a version of tetris 

#include "machine.h"


uint16_t *_MAP0=(uint16_t*)PPU_MAP0;	
uint16_t *_MAP1=(uint16_t*)PPU_MAP1;	
uint16_t *_CLUT=(uint16_t*)PPU_CLUT;	

#define SWAP_UINT16(x) (((x) >> 8) | ((x) << 8))

#include "tetris.h"
/* Shapes data */
const int shapes[7][4][4][2] =
{
     /* O */
     {
          {{0,0},{1,0},{0,1},{1,1}},
          {{0,0},{1,0},{0,1},{1,1}},
          {{0,0},{1,0},{0,1},{1,1}},
          {{0,0},{1,0},{0,1},{1,1}}
     },
     /* I */
     {
          {{1,0},{1,1},{1,2},{1,3}},
          {{0,1},{1,1},{2,1},{3,1}},
          {{1,0},{1,1},{1,2},{1,3}},
          {{0,1},{1,1},{2,1},{3,1}}
     },
     /* L */
     {
          {{0,1},{1,1},{2,1},{2,2}},
          {{1,0},{1,1},{1,2},{2,0}},
          {{0,0},{0,1},{1,1},{2,1}},
          {{1,0},{1,1},{1,2},{0,2}}
     },
     /* J */
     {
          {{1,0},{1,1},{1,2},{2,2}},
          {{0,2},{1,2},{2,2},{2,1}},
          {{0,0},{1,0},{1,1},{1,2}},
          {{0,1},{0,2},{1,1},{2,1}}
     },
     /* S */
     {
          {{1,1},{1,2},{2,0},{2,1}},
          {{0,1},{1,1},{1,2},{2,2}},
          {{1,1},{1,2},{2,0},{2,1}},
          {{0,1},{1,1},{1,2},{2,2}}
     },
     /* Z */
     {
          {{0,0},{0,1},{1,1},{1,2}},
          {{0,2},{1,1},{2,1},{1,2}},
          {{0,0},{0,1},{1,1},{1,2}},
          {{0,2},{1,1},{2,1},{1,2}}
     },
     /* T */
     {
          {{0,1},{1,0},{1,1},{1,2}},
          {{0,1},{1,1},{1,2},{2,1}},
          {{1,0},{1,1},{1,2},{2,1}},
          {{1,0},{0,1},{1,1},{2,1}}
     }
};

void
shape_set(void)
{
     int i, j;

     for(i = 0; i < 4; ++i)
          for(j = 0; j < EXP_FACT; ++j)
               frame[current.x + shapes[current.num][current.pos][i][0]]
                    [current.y + shapes[current.num][current.pos][i][1] * EXP_FACT + j]
                    = current.num + 1;

     if(current.x < 1)
          for(i = 0; i < FRAMEW + 1; ++i)
               frame[0][i] = Border;

     return;
}

void
shape_unset(void)
{
     int i, j;

     for(i = 0; i < 4; ++i)
          for(j = 0; j < EXP_FACT; ++j)
               frame[current.x + shapes[current.num][current.pos][i][0]]
                    [current.y + shapes[current.num][current.pos][i][1] * EXP_FACT + j] = 0;

     if(current.x < 1)
          for(i = 0; i < FRAMEW + 1; ++i)
               frame[0][i] = Border;
     return;
}

void
shape_new(void)
{
     int i;

     /* Draw the previous shape for it stay there */
     shape_set();
     check_plain_line();

     /* Set the new shape property */
     current.num = current.next;
     current.x = 1;
     current.y = (FRAMEW / 2) - 1;;
     current.next = nrand(0, 6);

     frame_nextbox_refresh();

     if(current.x > 1)
          for(i = 2; i < FRAMEW - 1; ++i)
               frame[1][i] = 0;

     return;
}

void
shape_go_down(void)
{

     shape_unset();

     /* Fall the shape else; collision with the ground or another shape
      * then stop it and create another */
     if(!check_possible_pos(current.x + 1, current.y))
          ++current.x;
     else
          if(current.x > 2)
               shape_new();
          else
          {
               shape_new();
               frame_refresh();
//               sleep(2);
               running = False;
          }




     return;
}

void
shape_set_position(int p)
{
     int old = current.pos;

     shape_unset();

     current.pos = p ;

     if(check_possible_pos(current.x, current.y))
          current.pos = old;

     return;
}


void
shape_move(int n)
{

     shape_unset();

     if(!check_possible_pos(current.x, current.y + n))
          current.y += n;

     return;
}

void
shape_drop(void)
{
     while(!check_possible_pos(current.x + 1, current.y))
     {
          shape_unset();
          ++current.x;
     }
     score += FRAMEH - current.x;

     return;
}

/* Functions */
void
init(void)
{
		/* Init variables */
		score = lines = 0;
		running = True;
		current.y = (FRAMEW / 2) - 1;
		current.num = nrand(0, 6);
		current.next = nrand(0, 6);
		return;
}

uint8_t lkey=0;

void get_key_event(void)
{
uint8_t key = IO_CONTROLLER;

	if (key&IO_LEFT)
		shape_move(-EXP_FACT);
	if (key&IO_RIGHT)
		shape_move(EXP_FACT);
	if (key&IO_DOWN)
		shape_drop();
	if (key&IO_A)
		shape_set_position(N_POS);
	if (key&IO_B)
		shape_set_position(P_POS);
	lkey=key;
}

void check_plain_line(void)
{
		int i, j, k, f, c = 0, nl = 0;

		for(i = 1; i < FRAMEH; ++i)
		{
				for(j = 1; j < FRAMEW; ++j)
							if(frame[i][j] == 0)
									++c;
				if(!c)
				{
							++nl;
							for(k = i - 1; k > 1; --k)
									for(f = 1; f < FRAMEW; ++f)
												frame[k + 1][f] = frame[k][f];
				}
				c = 0;
		}
		frame_refresh();
		return;
}

int
check_possible_pos(int x, int y)
{
     int i, j, c = 0;

     for(i = 0; i < 4; ++i)
          for(j = 0; j < EXP_FACT; ++j)
               if(frame[x + shapes[current.num][current.pos][i][0]]
                  [y + shapes[current.num][current.pos][i][1] * EXP_FACT + j] != 0)
                    ++c;

     return c;
}

void
set_color(int color)
{
     int bg = 0, fg = 0;

     switch(color)
     {
     default:
     case Black:   bg = 0;  break;
     case Blue:    bg = 1; break;
     case Red:     bg = 2; break;
     case Magenta: bg = 3; break;
     case White:   bg = 4; break;
     case Green:   bg = 5; break;
     case Cyan:    bg = 6; break;
     case Yellow:  bg = 7; break;
     case Border:  bg = 8; break;
     case Score:   fg = 9; bg = 10; break;
     }

//     printf("\e[%d;%dm", fg, bg);

     return;
}

void
printxy(int color, int x, int y, char *str)
{
	_MAP0[y+(x*64)]=(color&0xff);
}

int rand(void);
int
nrand(int min, int max)
{
int a=rand()&0x7;
	if (a<min) a=min;
	if (a>max) a=max;
	return a;
}


/* Shape attribute for draw it in the next box (center etc..)
 * [0]: +x
 * [1]: +y
 * [2]: What shape position choose for a perfect position in the box
 */
const int sattr[7][3] = {{0,2}, {-1,0}, {-1,1,1}, {-1,1}, {-1,1}, {0,1}, {0,1}};

void
frame_init(void)
{
     int i;

     /* Frame border */
     for(i = 0; i < FRAMEW + 1; ++i)
     {
          frame[0][i] = Border;
          frame[FRAMEH][i] = Border;
     }
     for(i = 0; i < FRAMEH; ++i)
     {
          frame[i][0] = Border;
          frame[i][1] = Border;
          frame[i][FRAMEW] = Border;
          frame[i][FRAMEW - 1] = Border;
     }

     frame_refresh();

     return;
}

void
frame_nextbox_init(void)
{
     int i;

     for(i = 0; i < FRAMEH_NB; ++i)
     {
          frame_nextbox[i][0] = Border;
          frame_nextbox[i][1] = Border;
          frame_nextbox[i][FRAMEW_NB - 1] = Border;
          frame_nextbox[i][FRAMEW_NB] = Border;

     }
     for(i = 0; i < FRAMEW_NB + 1; ++i)
          frame_nextbox[0][i] = frame_nextbox[FRAMEH_NB][i] = Border;

     frame_nextbox_refresh();

     return;
}

void
frame_refresh(void)
{
     int i, j;

     for(i = 0; i < FRAMEH + 1; ++i)
          for(j = 0; j < FRAMEW + 1; ++j)
                    printxy(frame[i][j], i, j, " ");
     return;
}

void
frame_nextbox_refresh(void)
{
     int i, j;

     /* Clean frame_nextbox[][] */
     for(i = 1; i < FRAMEH_NB; ++i)
          for(j = 2; j < FRAMEW_NB - 1; ++j)
               frame_nextbox[i][j] = 0;

     /* Set the shape in the frame */
     for(i = 0; i < 4; ++i)
          for(j = 0; j < EXP_FACT; ++j)
               frame_nextbox
                    [2 + shapes[current.next][sattr[current.next][2]][i][0] + sattr[current.next][0]]
                    [4 + shapes[current.next][sattr[current.next][2]][i][1] * EXP_FACT + j + sattr[current.next][1]]
                    = current.next + 1;

     /* Draw the frame */
     for(i = 0; i < FRAMEH_NB + 1; ++i)
          for(j = 0; j < FRAMEW_NB + 1; ++j)
               printxy(frame_nextbox[i][j], i, j + FRAMEW + 3, " ");

     return;
}




uint16_t irq_pos=224<<8;
uint16_t irq_move=-1;
uint16_t id=0;
uint8_t t=0;
volatile uint16_t move =0;

static unsigned long int next = 1;

unsigned int __mulsi3 (unsigned int a, unsigned int b)
{
  unsigned int r = 0;
  while (a)
	{
		if (a & 1)
			r += b;
		a >>= 1;
		b <<= 1;
	}
  return r;
}

int rand(void)
{
    next = next * (256+128+1) + 1;
    return (unsigned int)(next) & 0x7fff;
}

void srand(unsigned int seed)
{
    next = seed;
}

void nmi_cb()
{
	PPU_CTRL=PPU_MAP0_ON|PPU_MAP1_ON|1;
	t++;
	if ((t&7)==0)
	{
		move = 1;
	}
	_CLUT[0]=0x0000;
	PPU_SCANLINE=4;
	PPU_MAP0_X=0;
	PPU_MAP1_X+=1;

}

void irq_cb()
{
	PPU_SCANLINE+=1;
	_CLUT[0]+=0x0004;
}

void setup()
{
	int q,x,y;
	q=0;
	for (y=0;y<32;y++)
	{
		for (x=0;x<64;x++)
		{
			_MAP0[x+(y*64)]=0;
			_MAP1[x+(y*64)]=q&0x7ff;
			q++;
		}
	}
}

int main()
{
	PPU_TILES0=0;
	PPU_TILES1=0;
	PPU_BPP_MODE=0x33;
	setup();

	init();
	frame_init();
	frame_nextbox_init();;
	current.last_move = False;

	while (1)
	{
		if (move==1)
		{
			get_key_event();
			shape_set();
			frame_refresh();
			shape_go_down();
			move = 0;
		}
	}
	return 0;
}




