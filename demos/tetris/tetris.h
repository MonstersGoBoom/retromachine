#ifndef _TETRIS_H_
#define _TETRIS_H_

/* Expension factor of shapes */
#define EXP_FACT 1

/* Frame dimension */
#define FRAMEW (int)(22)
#define FRAMEH (int)(25)
#define FRAMEW_NB 15
#define FRAMEH_NB 5

/* Shapes position */
#define N_POS ((current.pos < 3) ? current.pos + 1 : 0)
#define P_POS ((current.pos > 0) ? current.pos - 1 : 3)

/* Draw the score.. */
//#define DRAW_SCORE() set_color(Score);                             \
     //printf("\033[%d;%dH %d", FRAMEH_NB + 3, FRAMEW + 10, score);   \
     //printf("\033[%d;%dH %d", FRAMEH_NB + 4, FRAMEW + 10, lines);   \
     //set_color(0);

/* Bool type */
typedef enum { False, True } Bool;

/* Shape structure */
typedef struct
{
     int num;
     int next;
     int pos;
     int x, y;
     Bool last_move;
} shape_t;

/* Color enum */
enum { Black, Blue, Red, Magenta, White, Green, Cyan, Yellow, Border, Score, ColLast };

/* Prototypes */

void printxy(int, int, int, char*);
void set_color(int);
int nrand(int, int);
void sig_handler(int);

void frame_init(void);
void frame_nextbox_init(void);
void frame_refresh(void);
void frame_nextbox_refresh(void);

void shape_set(void);
void shape_unset(void);
void shape_new(void);
void shape_go_down(void);
void shape_set_position(int);
void shape_move(int);
void shape_drop(void);

void arrange_score(int l);
void check_plain_line(void);
int check_possible_pos(int, int);
void get_key_event(void);

/* Variables */

const int shapes[7][4][4][2];
shape_t current;
int frame[FRAMEH + 1][FRAMEW + 1];
int frame_nextbox[FRAMEH_NB][FRAMEW_NB];
int score;
int lines;
Bool running;

#endif


