# Retromachine

Toy retro console. with a 6809 CPU. 

320x224 video resolution . 

2 planes 64x32 tiles each. 512 x 256 pixels 

4 bit 8x8 tiles. 

16 palette sets of 16 colors.

256 colors total of 4096 possible colors.

STILL VERY MUCH W.I.P. no sprites, no audio ( yet ) expect errors and bugs. 

this is a lunch time/spare time hacking thing. it works it's fast enough. 

The hope is to make something like this in an FPGA one day.

included GCC6809 windows build and AS6809 ASZ80 

see <Demos> folder 

# Building

Install RetroArch

clone this repo.

Type make.

this will build the assembler A09,and the .so file needed.

Type make demos 

will build all the demos currently available

Has been tested on Windows GCC and Arch Linux.

# Running 

retroarch -L bin\retromachine_libretro.so bin\name.bin

# bmpconv
bmpconv will load a 4 or 8 bit indexed bmp and extract tiles,clut,and map data to binary data. 

	-o<prefexname> 
		prefixname will create prefixname_clut.bin,prefixname_map.bin,prefixname_tiles.bin

	-p<planes>
		1,2,3,or 4 planes exported. 

	-m 
		output map

	-c 
		output clut 

	-r 
		remove repeated tiles

# Architecture

Memory Range | Description
------------ | -------------
0x0000-0xd000 | Program code and ram
0xd000-0xe000 | MAP0 64 x 32 tilemap of 16 bit ints 
0xe000-0xf000 | MAP1 64 x 32 tilemap of 16 bit ints 
0xf000-0xf200 | Color table 256 x 16 bit. 444 bit RGB

Video | Description
------------ | -------------
0xff00-0xff01 | 16 bit word pointer to Map0 Tiles data. ( inc. BANK ID *1)
0xff02-0xff03 | 16 bit word pointer to Map1 Tiles data. ( inc. BANK ID )
0xff04-0xff05 | 16 bit smooth scroll X MAP 0 
0xff06-0xff07 | 16 bit smooth scroll X MAP 1 
0xff08-0xff08 | 8 bit smooth scroll Y MAP 0
0xff09-0xff09 | 8 bit smooth scroll Y MAP 1
0xff12-0xff12 | READ 8 bit current raster line.WRITE next IRQ line
0xff13-0xff13 | Video Control FlagA ( see below *2 )
0xff14-0xff14 | Video Control FlagB ( see below *2 )

IO | Description
------------|-------------
0xff20			| Controller

Button | Value
------------|-------------
IO_SELECT | 1<<0
IO_START  |	1<<1
IO_UP 	  |	1<<2
IO_DOWN   |	1<<3
IO_LEFT   |	1<<4
IO_RIGHT  |	1<<5
IO_A 	  |	1<<6
IO_B 	  |	1<<7

COPY CHIP | Description
--------------|------------
0xff80-0xff81 | Copy chip source. Copy from address pointed to here. Including BANK.
0xff82-0xff83 | Copy chip destination. Store into address pointed to here.
0xff84-0xff85 | Amount of Data to copy. Writing to this register will begin the copy.
......-...... | Read value will be amount of data remaining in the copy operation.
0xff86-0xff87 | Source address increment. (*3)
0xff88-0xff89 | Destination address increment. (*3) 

RESET VECTORS | Description
--------------|------------
0xfffa-0xfffb | address of IRQ function 
0xfffc-0xfffd | address of NMI ( called once per frame ) 
0xfffe-0xffff | reset vector . address of program start.

BANKS | 64kb blocks
--------------|------------
0x10000-0x1ffff | BANK 1 64kb rom
0x20000-0x2ffff | BANK 2 ""
0x30000-0x3ffff | BANK 3 ""

*1 

Tiles and Sprites can be stored in rom and rendered directly by the PPU.

This means you can have up to 192kb of data. 

you can also copy from the ROMS into RAM with the copy chip documented above.

The example asm file contains a 256 x 32 map which is copied column by column via the copy chip.

To update the offscreen scroll edge.

*2
Video Control Flag A

BITS     | FLAGS
---------|------
00000011 | Bank Select. 0-3.
00010000 | MAP 0 Enabled.
00100000 | MAP 1 Enabled.
01000000 | SPRITES Enabled.
10000000 | Map 1 is text.


Video Control Flag B bits per pixel select.

Two Nybbles one for Map1 one for Map0 

00 - 1 bit per pixel.

01 - 2 bit per pixel.

10 - 3 bit per pixel. 

11 - 4 bit per pixel. 

EG. 
0x33 will set both maps to 4 bpp 
0x30 will set map to 4 bit and one map to 1 bit 

*3 Copy source & destination increment.

for each word copied. add this to next read/write.eg 256 would add 256 words per iteration.   

If Map1 is enabled and Map1 text is set 

Map1 will be rendered as a 6x7 grid of bytes vs 8x8 of words to enable text mode.
this map will take priority over Map0. I.e. rendered last. ( MSX ) 

In your Retroarch.cfg 
```video_smooth = "false"```

![Image of example](screenshots/out-181031-221244.png)
![Image of text](screenshots/test.font-181031-221632.png)


# Credits

A09 assembler (C) Copyright 1993,1994 L.C. Benschop.  Parts (C) Copyright 2001-2016 H. Seib. 

#Links

[6809 Books online](http://www.colorcomputerarchive.com/coco/Documents/Microprocessors/MC6809/)









